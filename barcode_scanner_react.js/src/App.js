App.js

import Barcode  from './Barcode';
import './App.css';
 
function App() {
  return (
    <div className="App">
      <Barcode />
    </div>
  );
}
 
export default App;